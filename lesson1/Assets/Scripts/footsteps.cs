﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;


public class footsteps : MonoBehaviour
{
    AudioSource footstepsAudioSorce;
    public AudioClip[] walkingclips;
    public AudioClip[] runningclips;
    vThirdPersonInput tpInput;
    vThirdPersonController tpController;
    public float VolumeMin = 1f;
    public float pitchMin = 1f;
    public float pitchMax = 1f;
    int LastIndex;
    int NewIndex;






    void Start()
    {
        footstepsAudioSorce = gameObject.AddComponent<AudioSource>();
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
        //footstepsAudioSorce = gameObject.AddComponent<AudioSource>();
        //footstepsAudioSorce.clip = footstepsAudioClip();
    }

    void footstep()

    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            footstepsAudioSorce.volume = Random.Range(VolumeMin, 1f);
            footstepsAudioSorce.pitch = Random.Range(pitchMin, pitchMax);

            if (tpController.isSprinting)
            {

                Randomization(runningclips.Length);
                footstepsAudioSorce.PlayOneShot(runningclips[NewIndex]);
                LastIndex = NewIndex;
            }

            else
            {
                Randomization(walkingclips.Length);
                footstepsAudioSorce.PlayOneShot(runningclips[NewIndex]);
                LastIndex = NewIndex;
            }
        }
    }

    void Randomization(int cliplenght)
    {
        NewIndex = Random.Range(0, cliplenght);
        while (NewIndex == LastIndex)
            NewIndex = Random.Range(0, cliplenght);
    }
    // Update is called once per frame
    void Update()
    {

    }

    
   //void footstep()
    //{
        //int ClipIndex;
        //ClipIndex = Random.Range(0, 7);
       // Randomization();
        //footstepsAudioSorce.volume = Random.Range(0.7f, 1f);
        //footstepsAudioSorce.pitch = Random.Range(0.6f, 1.2f);

        //footstepsAudioSorce.PlayOneShot(runningclips[NewIndex]);
        //Debug.Log(message: "Step#" + NewIndex);
        //LastIndex = NewIndex;

    //}

    //void Randomization()
    //{
      //  NewIndex = Random.Range(0, runningclips.Length);
        //while (NewIndex == LastIndex)
          //  NewIndex = Random.Range(0, runningclips.Length);
   // }
}
