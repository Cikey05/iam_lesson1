﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : MonoBehaviour
{

    AudioSource JumpAudioSource;
    public AudioClip JumpAudioClip;


    // Start is called before the first frame update
    void Start()
    {

       JumpAudioSource = gameObject.AddComponent<AudioSource>;
       
    }

    void jumpup()
    {
        JumpAudioSource.PlayOneShot(JumpAudioClip);
    }



    void Update()
    {
       
    }

   
}
